package com.peliza.console.impl;

import com.peliza.Peliza;
import com.peliza.console.abstraction.Consolable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author start
 */
public class Console implements Consolable {


    @Override
    public void start(String path) {
        Peliza eliza = new Peliza(path);
        boolean NotExit=true;
        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Witam w czym mogę ci pomóc? ");
        while (NotExit){
            try {
                String s=br.readLine();
                if(s.equals("exit")){
                    NotExit=false;
                    break;
                }
                String ans=eliza.answer(s);
                System.out.println(ans);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}

