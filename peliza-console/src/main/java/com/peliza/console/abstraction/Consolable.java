package com.peliza.console.abstraction;

public interface Consolable {
    void start(String path);
}
