package com.peliza.console;

import com.peliza.console.impl.Console;

/**
 * @autor start
 */
public class Application {
    public static void main(String[] args) {
        if (args.length == 0) {
            new Console().start(null);
        } else if (args.length == 1) {
            new Console().start(args[0]);
        } else {
            System.out.print("Uruchomienie aplikacji nie powiodlo się: zła liczba argumentów.");
        }
    }
}
