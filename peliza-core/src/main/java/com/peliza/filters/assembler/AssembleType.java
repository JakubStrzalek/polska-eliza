package com.peliza.filters.assembler;

public enum AssembleType {
    FROM_PROPERTIES,
    PREDEFINED
}
