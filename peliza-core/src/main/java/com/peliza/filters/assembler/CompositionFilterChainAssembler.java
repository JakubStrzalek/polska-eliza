package com.peliza.filters.assembler;

import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.peliza.filters.model.Filter;
import com.peliza.filters.model.FiltersFactory;
import com.peliza.properties.loader.FilterConfigurationLoader;
import com.peliza.properties.loader.PropertiesLoaderFactory;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Do budowy korzystamy z wzorca kompozyt (kazdy z filtrow bedzie mial odwolanie do nastepnika).
 *
 * @author jstrzalek
 */
public class CompositionFilterChainAssembler implements FilterChainAssembler {

    final private FilterConfigurationLoader filterConfigurationLoader;

    final private FiltersFactory filtersFactory;

    final private FilterChainBuilder filterChainBuilder;

    @AssistedInject
    public CompositionFilterChainAssembler(@Assisted String fileUrl, PropertiesLoaderFactory propertiesLoaderFactory, FiltersFactory filtersFactory, FilterChainBuilder filterChainBuilder) {
        this.filtersFactory = filtersFactory;
        this.filterChainBuilder = filterChainBuilder;
        this.filterConfigurationLoader = propertiesLoaderFactory.create(fileUrl);
    }

    @AssistedInject
    public CompositionFilterChainAssembler(PropertiesLoaderFactory propertiesLoaderFactory, FiltersFactory filtersFactory, FilterChainBuilder filterChainBuilder) {
        this.filtersFactory = filtersFactory;
        this.filterChainBuilder = filterChainBuilder;
        this.filterConfigurationLoader = propertiesLoaderFactory.create();
    }

    @Override
    public Filter assemble(AssembleType type) {
        switch (type) {
            case FROM_PROPERTIES:
                throw new NotImplementedException();
            case PREDEFINED:
                return createPreDefined();
        }

        throw new IllegalArgumentException();
    }

    private Filter createPreDefined() {
        return filterChainBuilder
                .withBefore(filtersFactory.createStatic(filterConfigurationLoader))
                .withBefore(filtersFactory.createToPelizaReader(filterConfigurationLoader))
                .withBefore(filtersFactory.createSubjectReminder(filterConfigurationLoader))
                .withBefore(filtersFactory.createAboutUser(filterConfigurationLoader))
                .withBefore(filtersFactory.createMoodChecker(filterConfigurationLoader))
                .build();

    }


}
