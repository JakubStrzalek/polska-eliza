package com.peliza.filters.assembler;

import com.peliza.filters.model.Filter;

public interface FilterChainAssembler {
    Filter assemble(AssembleType type);
}
