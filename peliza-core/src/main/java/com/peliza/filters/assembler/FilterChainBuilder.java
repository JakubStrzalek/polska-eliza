package com.peliza.filters.assembler;

import com.peliza.filters.model.Filter;

public class FilterChainBuilder {
    private Filter currentFilter;

    FilterChainBuilder() {
        this.currentFilter = null;
    }

    public FilterChainBuilder withBefore(Filter filter) {
        Filter lastFilter = currentFilter;
        currentFilter = filter;
        if (lastFilter != null) {
            lastFilter.setPrev(currentFilter);
        }
        currentFilter.setNext(lastFilter);

        return this;
    }

    Filter build() {
        if (currentFilter == null) {
            throw new IllegalStateException();
        }
        return currentFilter;
    }
}
