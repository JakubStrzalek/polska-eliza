package com.peliza.filters.assembler;

public interface FilterChainAssemblerFactory {
    FilterChainAssembler createFilterChainAssembler(String fileUrl);

    FilterChainAssembler createFilterChainAssembler();
}
