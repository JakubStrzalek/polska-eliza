package com.peliza.filters.model;

import com.peliza.properties.loader.FilterConfigurationLoader;

import javax.inject.Named;

public interface FiltersFactory {
    @Named("StaticFilter")
    Filter createStatic(FilterConfigurationLoader loader);

    @Named("ToPelizaReaderFilter")
    Filter createToPelizaReader(FilterConfigurationLoader loader);

    @Named("MoodCheckerFilter")
    Filter createMoodChecker(FilterConfigurationLoader loader);

    @Named("AboutUserFilter")
    Filter createAboutUser(FilterConfigurationLoader loader);

    @Named("SubjectReminderFilter")
    Filter createSubjectReminder(FilterConfigurationLoader filterConfigurationLoader);
}
