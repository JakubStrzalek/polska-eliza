package com.peliza.filters.model;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.peliza.communication.request.Request;
import com.peliza.communication.session.Session;
import com.peliza.messages.model.Message;
import com.peliza.messages.model.Word;
import com.peliza.properties.loader.FilterConfigurationLoader;
import com.peliza.properties.model.FilterConfiguration;
import com.peliza.utils.RandomListAccess;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class MoodCheckerFilter extends AbstractFilter {

    final private Map<String, RandomListAccess<String>> randomListAccessMap;

    @Inject
    public MoodCheckerFilter(@Assisted FilterConfigurationLoader filterConfigurationLoader) {
        FilterConfiguration filterConfiguration = filterConfigurationLoader.loadFilterConfiguration(this.getClass().getSimpleName());
        this.randomListAccessMap = filterConfiguration.loadCustomResponses();

    }

    @Override
    protected String invoke(Session<Message> session, Request<Message> request) {
        Message message = request.getRequestedObject();
        Collection<Word> words = message.getTokenized();
        Optional<String> mood = moodWithinMessage(words);

        if (mood.isPresent()) {
            return formatResponse(mood.get());
        }

        return PROPAGATE_TO_NEXT;
    }

    private String formatResponse(String moodKey) {
        RandomListAccess<String> randomListAccess = randomListAccessMap.get(moodKey);
        return randomListAccess.get();
    }

    private Optional<String> moodWithinMessage(Collection<Word> words) {
        Set<String> moods = this.randomListAccessMap.keySet();


        return moods.stream()
                .filter(
                        m -> words.stream()
                                .anyMatch(
                                        w -> w.getLemma() != null && w.getLemma().contains(m)

                                )
                )
                .findFirst();

    }


}
