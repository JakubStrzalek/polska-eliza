package com.peliza.filters.model.statefull;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.peliza.communication.request.Request;
import com.peliza.communication.session.Session;
import com.peliza.filters.model.AbstractFilter;
import com.peliza.filters.model.Filter;
import com.peliza.messages.model.Message;
import com.peliza.properties.loader.FilterConfigurationLoader;
import com.peliza.properties.model.FilterConfiguration;
import com.peliza.utils.RandomListAccess;

public class SubjectReminderFilter extends AbstractFilter {

    private final Integer threshholdNotToAnswer;
    private final RandomListAccess<String> backToQuestionAnswers;
    private Integer state;

    @Inject
    public SubjectReminderFilter(@Assisted FilterConfigurationLoader loader) {
        FilterConfiguration configuration = loader.loadFilterConfiguration(this.getClass().getSimpleName());
        backToQuestionAnswers = configuration.loadResponses();
        threshholdNotToAnswer = Integer.parseInt(configuration.loadProperty("WaitToRemind"));
        state = 0;
    }

    @Override
    protected String invoke(Session<Message> session, Request<Message> request) {
        if (state.equals(threshholdNotToAnswer)) {
            state = 0;
            return lastAnsweredResponsePreviousFilters(session);
        }
        state++;
        return null;
    }

    private String lastAnsweredResponsePreviousFilters(Session<Message> session) {
        Filter filter = this;
        String response = null;

        while (filter != null && response == null) {
            filter = filter.getPrev();
            response = session.getLastAnswer(filter);
        }

        return formatResponse(response);
    }

    private String formatResponse(String response) {
        if (response == null) {
            return null;
        }

        return backToQuestionAnswers.get() + response;
    }


}
