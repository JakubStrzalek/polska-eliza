package com.peliza.filters.model;

import com.peliza.communication.request.Request;
import com.peliza.communication.session.Session;
import com.peliza.messages.model.Message;

public interface Filter {
    String service(Session<Message> session, Request<Message> request);

    Filter getNext();

    void setNext(Filter filter);

    Filter getPrev();

    void setPrev(Filter filter);
}
