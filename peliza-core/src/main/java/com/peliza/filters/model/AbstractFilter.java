package com.peliza.filters.model;

import com.peliza.communication.request.Request;
import com.peliza.communication.session.Session;
import com.peliza.messages.model.Message;
import com.peliza.utils.ExceptionCommunicates;

public abstract class AbstractFilter implements Filter {

    protected final String PROPAGATE_TO_NEXT = null;
    private Filter next;
    private Filter prev;

    @Override
    public String service(Session<Message> session, Request<Message> request) {
        String response = this.invoke(session, request);
        if (response != null) {
            session.updateLastAnswer(this, response);
            return response;
        }

        return passToNext(session, request);
    }

    final private String passToNext(Session<Message> session, Request<Message> request) {
        if (next == null)
            throw new IllegalStateException(ExceptionCommunicates.SHOULD_NOT_PROPAGATE_TO_NEXT);

        return next.service(session, request);
    }

    protected abstract String invoke(Session<Message> session, Request<Message> request);

    @Override
    public Filter getNext() {
        return next;
    }

    @Override
    public void setNext(Filter next) {
        this.next = next;
    }

    @Override
    public Filter getPrev() {
        return prev;
    }

    @Override
    public void setPrev(Filter prev) {
        this.prev = prev;
    }

}
