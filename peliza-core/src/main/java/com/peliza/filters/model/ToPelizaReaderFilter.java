package com.peliza.filters.model;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.peliza.communication.request.Request;
import com.peliza.communication.session.Session;
import com.peliza.messages.model.Message;
import com.peliza.messages.model.Tag;
import com.peliza.messages.model.Word;
import com.peliza.messages.model.tagProperties.PersonVerb;
import com.peliza.properties.loader.FilterConfigurationLoader;
import com.peliza.properties.model.FilterConfiguration;
import com.peliza.utils.RandomListAccess;

import java.util.Collection;
import java.util.function.Predicate;

/**
 * Filtr odpowiedzialny za wykrycie zwrotow do Pelizy
 *
 * @author jstrzalek
 */
public class ToPelizaReaderFilter extends AbstractFilter {

    final private RandomListAccess<String> acceptableAnswers;

    @Inject
    public ToPelizaReaderFilter(@Assisted FilterConfigurationLoader filterConfigurationLoader) {
        FilterConfiguration configuration = filterConfigurationLoader.loadFilterConfiguration(this.getClass().getSimpleName());
        acceptableAnswers = configuration.loadResponses();
    }

    @Override
    protected String invoke(Session<Message> session, Request<Message> request) {
        Message message = request.getRequestedObject();
        Collection<Word> words = message.getTokenized();

        if (messageToPeliza(words)) {
            return acceptableAnswers.get();
        }

        return PROPAGATE_TO_NEXT;
    }

    private boolean messageToPeliza(Collection<Word> words) {
        Predicate<Word> matchVerbAndSec = w ->
        {
            Tag tag = w.getTag();
            return tag.getPartOfSpeech() == Tag.PartOfSpeech.VERB
                    && tag.getProperty(Tag.Model.personVerb).equals(PersonVerb.p2nd.toString());
        };

        return words.stream().anyMatch(matchVerbAndSec);
    }
}
