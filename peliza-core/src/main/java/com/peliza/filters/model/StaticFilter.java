package com.peliza.filters.model;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.peliza.communication.request.Request;
import com.peliza.communication.session.Session;
import com.peliza.messages.model.Message;
import com.peliza.properties.loader.FilterConfigurationLoader;
import com.peliza.properties.model.FilterConfiguration;
import com.peliza.utils.RandomListAccess;

public class StaticFilter extends AbstractFilter {

    final private RandomListAccess<String> acceptableAnswers;

    @Inject
    public StaticFilter(@Assisted FilterConfigurationLoader filterConfigurationLoader) {
        FilterConfiguration filterConfiguration = filterConfigurationLoader.loadFilterConfiguration(this.getClass().getSimpleName());
        acceptableAnswers = filterConfiguration.loadResponses();
    }

    @Override
    protected String invoke(Session<Message> session, Request<Message> request) {
        return acceptableAnswers.get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StaticFilter that = (StaticFilter) o;

        return !(acceptableAnswers != null ? !acceptableAnswers.equals(that.acceptableAnswers) : that.acceptableAnswers != null);

    }

    @Override
    public int hashCode() {
        return acceptableAnswers != null ? acceptableAnswers.hashCode() : 0;
    }
}
