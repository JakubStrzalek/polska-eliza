package com.peliza.filters.model;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.peliza.communication.request.Request;
import com.peliza.communication.session.Session;
import com.peliza.messages.model.Message;
import com.peliza.messages.model.Tag;
import com.peliza.messages.model.Word;
import com.peliza.messages.model.tagProperties.PersonVerb;
import com.peliza.messages.services.transform.MessageAnalyzer;
import com.peliza.properties.loader.FilterConfigurationLoader;
import com.peliza.properties.model.FilterConfiguration;
import com.peliza.utils.RandomListAccess;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Wykrywa czy użytkownik wspomniał coś o sobie i podstawia do zdania
 */
public class AboutUserFilter extends AbstractFilter {


    final private String LENGTH_BETWEEN_NOUN_AND_VERB = "NounAndVerbMaxSpace";

    final private RandomListAccess<String> acceptableAnswers;

    final private MessageAnalyzer analyzer;

    private Integer lengthBetweenNounAndVerb;

    @Inject
    public AboutUserFilter(@Assisted FilterConfigurationLoader filterConfigurationLoader, MessageAnalyzer analyzer) {
        this.analyzer = analyzer;
        FilterConfiguration filterConfiguration = filterConfigurationLoader.loadFilterConfiguration(this.getClass().getSimpleName());
        acceptableAnswers = filterConfiguration.loadResponses();
        String property = filterConfiguration.loadProperty(LENGTH_BETWEEN_NOUN_AND_VERB);
        lengthBetweenNounAndVerb = Integer.parseInt(property);
    }

    @Override
    protected String invoke(Session<Message> session, Request<Message> request) {
        Message message = request.getRequestedObject();
        Collection<Word> words = message.getTokenized();

        Optional<Word> verb = messageToUser(words);

        if (verb.isPresent()) {
            Optional<Word> noun = findNextNounToVerb(words, verb.get(), lengthBetweenNounAndVerb);
            if (!noun.isPresent())
                return PROPAGATE_TO_NEXT;
            return formatResponse(verb.get(), noun.get());
        }

        return PROPAGATE_TO_NEXT;
    }

    private Optional<Word> messageToUser(Collection<Word> words) {
        Predicate<Word> matchVerbAndFirst = w ->
        {
            Tag tag = w.getTag();
            return tag.getPartOfSpeech() == Tag.PartOfSpeech.VERB
                    && tag.getProperty(Tag.Model.personVerb).equals(PersonVerb.p1st.toString());
        };

        return words.stream()
                .filter(matchVerbAndFirst)
                .findFirst();
    }

    private String formatResponse(Word verb, Word noun) {
        Word changedWord = analyzer.changeVerbTo2ndFrom1stForm(verb);

        String template = acceptableAnswers.get();

        return String.format(template, verb.getOriginal().toLowerCase(), noun.getOriginal().toLowerCase());

    }

    private Optional<Word> findNextNounToVerb(final Collection<Word> words,
                                              final Word verb,
                                              final int minBetweenWords) {

        Optional<Integer> margin = Optional.empty();

        for (Word word : words) {
            if (word.equals(verb)) {
                margin = Optional.of(0);
            } else {
                if (margin.isPresent()) {
                    if (margin.get() <= minBetweenWords) {
                        if (word.getTag().getPartOfSpeech() == Tag.PartOfSpeech.NOUN) {
                            return Optional.of(word);
                        }
                    } else {
                        return Optional.empty();
                    }
                }
            }
        }

        return Optional.empty();
    }
}
