package com.peliza.filters;

import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.name.Names;
import com.peliza.filters.assembler.CompositionFilterChainAssembler;
import com.peliza.filters.assembler.FilterChainAssembler;
import com.peliza.filters.assembler.FilterChainAssemblerFactory;
import com.peliza.filters.model.*;
import com.peliza.filters.model.statefull.SubjectReminderFilter;
import com.peliza.properties.PropertiesModuleConfiguration;

/**
 * Konfiguracja Google Guice dla modulu Filtrow
 *
 * @author jstrzalek
 */
public class FiltersModuleConfiguration extends AbstractModule {
    @Override
    protected void configure() {
        install(new PropertiesModuleConfiguration());

        FactoryModuleBuilder factoryModuleBuilder = new FactoryModuleBuilder();
        install(factoryModuleBuilder
                .implement(Filter.class, Names.named("StaticFilter"), StaticFilter.class)
                .implement(Filter.class, Names.named("ToPelizaReaderFilter"), ToPelizaReaderFilter.class)
                .implement(Filter.class, Names.named("AboutUserFilter"), AboutUserFilter.class)
                .implement(Filter.class, Names.named("SubjectReminderFilter"), SubjectReminderFilter.class)
                .implement(Filter.class, Names.named("MoodCheckerFilter"), MoodCheckerFilter.class)
                .build(FiltersFactory.class));
        install(factoryModuleBuilder
                .implement(FilterChainAssembler.class, CompositionFilterChainAssembler.class)
                .build(FilterChainAssemblerFactory.class));
    }

}
