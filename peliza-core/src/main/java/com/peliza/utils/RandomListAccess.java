package com.peliza.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class RandomListAccess<T> {
    final private List<T> collection;
    final private Random random;

    public RandomListAccess(Collection<T> collection) {
        this.collection = new ArrayList<>(collection);
        this.random = new Random();
    }

    public T get() {
        return collection.get(random.nextInt(collection.size()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RandomListAccess<?> that = (RandomListAccess<?>) o;

        if (collection != null ? !collection.equals(that.collection) : that.collection != null) return false;
        return !(random != null ? !random.equals(that.random) : that.random != null);

    }

    @Override
    public int hashCode() {
        int result = collection != null ? collection.hashCode() : 0;
        result = 31 * result + (random != null ? random.hashCode() : 0);
        return result;
    }
}
