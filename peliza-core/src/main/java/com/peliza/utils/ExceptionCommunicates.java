package com.peliza.utils;

/**
 * @autor start
 */
public class ExceptionCommunicates {
    public final static String SHOULD_NOT_PROPAGATE_TO_NEXT = "Current Filter should not pass Message to other Filter without set next Filter!";
    public final static String NO_RAW_MESSAGE_IN_REQUEST = "Current request doesn't content request Messaged!";
    public final static String WRONG_INIT_OR_TAG_PROPERTY_TYPE = "It's look like you forgot to initialize property or used wrong property type!";
}
