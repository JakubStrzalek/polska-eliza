package com.peliza.communication.request;

import com.peliza.messages.model.Message;

public interface RequestFactory {
    Request<Message> create(Message domain);
}
