package com.peliza.communication.request;

public interface Request<T> {
    T getRequestedObject();

}
