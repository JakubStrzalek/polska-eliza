package com.peliza.communication.request;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.peliza.messages.model.Message;

public class MessageRequest implements Request<Message> {

    final private Message message;

    @Inject
    MessageRequest(@Assisted Message message) {
        this.message = message;
    }

    @Override
    public Message getRequestedObject() {
        return message;
    }
}
