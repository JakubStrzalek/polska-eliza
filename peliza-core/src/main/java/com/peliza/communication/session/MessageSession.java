package com.peliza.communication.session;

import com.peliza.filters.model.Filter;
import com.peliza.messages.model.Message;

import java.util.HashMap;
import java.util.Map;

public class MessageSession implements Session<Message> {

    private Map<String, String> lastFiltersAnswers;

    public MessageSession() {
        lastFiltersAnswers = new HashMap<>();
    }

    @Override
    public String getLastAnswer(Filter filter) {
        return (filter == null ? null : lastFiltersAnswers.get(filter.getClass().getSimpleName()));
    }

    @Override
    public void updateLastAnswer(Filter filter, String message) {
        lastFiltersAnswers.put(filter.getClass().getSimpleName(), message);
    }
}
