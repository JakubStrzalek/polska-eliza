package com.peliza.communication.session;

import com.peliza.filters.model.Filter;

public interface Session<T> {

    String getLastAnswer(Filter filter);

    void updateLastAnswer(Filter filter, String message);
}
