package com.peliza.communication;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.peliza.communication.request.MessageRequest;
import com.peliza.communication.request.Request;
import com.peliza.communication.request.RequestFactory;
import com.peliza.communication.session.MessageSession;
import com.peliza.communication.session.Session;
import com.peliza.messages.model.Message;

public class CommunicationModuleConfiguration extends AbstractModule {
    @Override
    protected void configure() {
        bind(new TypeLiteral<Session<Message>>() {
        }).to(MessageSession.class);
        FactoryModuleBuilder factoryModuleBuilder = new FactoryModuleBuilder();
        install(factoryModuleBuilder.implement(new TypeLiteral<Request<Message>>() {
        }, MessageRequest.class)
                .build(RequestFactory.class));
    }
}
