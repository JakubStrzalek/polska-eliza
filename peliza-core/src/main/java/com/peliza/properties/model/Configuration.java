package com.peliza.properties.model;

import javax.xml.bind.annotation.*;
import java.util.LinkedList;

@XmlRootElement(name = "config")
@XmlAccessorType(XmlAccessType.FIELD)
public class Configuration {

    @XmlElementWrapper(name = "filters")
    @XmlElement(name = "filter")
    private LinkedList<FilterConfiguration> filterConfigurations;

    public LinkedList<FilterConfiguration> getFilterConfigurations() {
        return filterConfigurations;
    }

    public void setFilterConfigurations(LinkedList<FilterConfiguration> filterConfigurations) {
        this.filterConfigurations = filterConfigurations;
    }
}
