package com.peliza.properties.model;

import javax.xml.bind.annotation.*;
import java.util.LinkedList;

@XmlRootElement(name = "messages")
@XmlAccessorType(XmlAccessType.FIELD)
public class Messages {
    @XmlAttribute
    private String id;

    @XmlElement(name = "message")
    private LinkedList<String> messages;

    public LinkedList<String> getMessages() {
        return messages;
    }

    public void setMessages(LinkedList<String> messages) {
        this.messages = messages;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
