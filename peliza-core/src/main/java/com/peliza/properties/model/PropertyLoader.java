package com.peliza.properties.model;

import com.peliza.utils.RandomListAccess;

import java.util.Map;

public interface PropertyLoader {
    RandomListAccess<String> loadResponses();

    Map<String, RandomListAccess<String>> loadCustomResponses();

    String loadProperty(String propertyName);
}
