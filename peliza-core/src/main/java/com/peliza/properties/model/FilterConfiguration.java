package com.peliza.properties.model;

import com.peliza.utils.RandomListAccess;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

@XmlRootElement(name = "filter")
@XmlAccessorType(XmlAccessType.FIELD)
public class FilterConfiguration implements PropertyLoader {

    @XmlElement
    private String name;

    @XmlElement(name = "property")
    private LinkedList<Property> properties;

    @XmlElement
    private LinkedList<Messages> messages;

    public LinkedList<Messages> getMessages() {
        return messages;
    }

    public void setMessages(LinkedList<Messages> messages) {
        this.messages = messages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedList<Property> getProperties() {
        return properties;
    }

    public void setProperties(LinkedList<Property> properties) {
        this.properties = properties;
    }

    @Override
    public RandomListAccess<String> loadResponses() {
        Map<String, RandomListAccess<String>> map = loadCustomResponses();
        return map.get("default");
    }

    @Override
    public Map<String, RandomListAccess<String>> loadCustomResponses() {
        Map<String, RandomListAccess<String>> map = new HashMap<>();
        messages.stream().forEach(m -> map.put(m.getId(), new RandomListAccess<>(m.getMessages())));
        return map;
    }

    @Override
    public String loadProperty(String propertyName) {
        Optional<Property> property = properties.stream().filter(p -> p.getName().equals(propertyName)).findFirst();
        if (property.isPresent()) {
            return property.get().getValue();
        } else
            throw new IllegalArgumentException();
    }

    private Map<String, RandomListAccess<String>> changeModel(Map<String, List<String>> map) {
        Map<String, RandomListAccess<String>> changedMap = new HashMap<>();
        map.keySet().stream()
                .forEach(
                        k -> changedMap.put(k, new RandomListAccess<>(map.get(k)))
                );

        return changedMap;
    }
}
