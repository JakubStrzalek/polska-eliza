package com.peliza.properties;

import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.name.Names;
import com.peliza.properties.loader.FilterConfigurationLoader;
import com.peliza.properties.loader.FilterConfigurationLoaderImpl;
import com.peliza.properties.loader.PropertiesLoaderFactory;
import com.peliza.properties.loader.strategies.LoadFilterConfigurationFromXml;
import com.peliza.properties.loader.strategies.LoadFilterConfigurationPredefined;
import com.peliza.properties.loader.strategies.LoadFilterConfigurationStrategy;
import com.peliza.properties.loader.strategies.LoadPropertiesStrategyFactory;

public class PropertiesModuleConfiguration extends AbstractModule {
    @Override
    protected void configure() {
        FactoryModuleBuilder factoryModuleBuilder = new FactoryModuleBuilder();
        install(factoryModuleBuilder
                .implement(LoadFilterConfigurationStrategy.class, Names.named("StrategyPredefined"), LoadFilterConfigurationPredefined.class)
                .implement(LoadFilterConfigurationStrategy.class, Names.named("StrategyFromXml"), LoadFilterConfigurationFromXml.class)
                .build(LoadPropertiesStrategyFactory.class));
        install(factoryModuleBuilder
                .implement(FilterConfigurationLoader.class, FilterConfigurationLoaderImpl.class)
                .build(PropertiesLoaderFactory.class));
    }
}
