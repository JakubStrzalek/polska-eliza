package com.peliza.properties.loader.strategies;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.peliza.properties.model.Configuration;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class LoadFilterConfigurationFromXml extends AbstractLoadFilterConfiguration implements LoadFilterConfigurationStrategy {

    private Configuration configuration;

    @Inject
    public LoadFilterConfigurationFromXml(@Assisted File file) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Configuration.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        this.configuration = (Configuration) jaxbUnmarshaller.unmarshal(file);
    }

    @Override
    public Configuration getConfiguration() {
        return configuration;
    }
}
