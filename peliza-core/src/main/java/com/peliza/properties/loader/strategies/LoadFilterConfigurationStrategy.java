package com.peliza.properties.loader.strategies;

import com.peliza.properties.model.FilterConfiguration;

public interface LoadFilterConfigurationStrategy {
    FilterConfiguration loadResponses(String key);
}
