package com.peliza.properties.loader.strategies;

import com.peliza.properties.model.Configuration;
import com.peliza.properties.model.FilterConfiguration;

import java.util.Optional;

public abstract class AbstractLoadFilterConfiguration implements LoadFilterConfigurationStrategy {

    @Override
    public FilterConfiguration loadResponses(String key) {
        Optional<FilterConfiguration> result = getConfiguration().getFilterConfigurations().stream()
                .filter(
                        f -> f.getName().equals(key)
                ).findFirst();

        if (result.isPresent())
            return result.get();
        else
            throw new IllegalArgumentException();
    }

    public abstract Configuration getConfiguration();
}
