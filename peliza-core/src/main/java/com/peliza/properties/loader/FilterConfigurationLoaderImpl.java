package com.peliza.properties.loader;

import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.peliza.properties.loader.strategies.LoadFilterConfigurationStrategy;
import com.peliza.properties.loader.strategies.LoadPropertiesStrategyFactory;
import com.peliza.properties.model.FilterConfiguration;

import java.io.File;

public class FilterConfigurationLoaderImpl implements FilterConfigurationLoader {

    private LoadFilterConfigurationStrategy loadStrategy;

    @AssistedInject
    public FilterConfigurationLoaderImpl(LoadPropertiesStrategyFactory loadPropertiesStrategyFactory, @Assisted String fileUrl) {
        this.loadStrategy = loadPropertiesStrategyFactory.createStrategyFromXml(new File(fileUrl));

    }

    @AssistedInject
    public FilterConfigurationLoaderImpl(LoadPropertiesStrategyFactory loadPropertiesStrategyFactory) {
        this.loadStrategy = loadPropertiesStrategyFactory.createStrategyPredefined();
    }


    @Override
    public FilterConfiguration loadFilterConfiguration(String filterName) {
        return loadStrategy.loadResponses(filterName);
    }



}
