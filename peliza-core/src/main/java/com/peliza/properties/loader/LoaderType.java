package com.peliza.properties.loader;

public enum LoaderType {
    FROM_PROPERTIES,
    PREDEFINED
}
