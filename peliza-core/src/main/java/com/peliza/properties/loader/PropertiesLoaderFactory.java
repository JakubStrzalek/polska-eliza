package com.peliza.properties.loader;

public interface PropertiesLoaderFactory {
    FilterConfigurationLoader create(String fileUrl);

    FilterConfigurationLoader create();
}
