package com.peliza.properties.loader.strategies;

import javax.inject.Named;
import java.io.File;

public interface LoadPropertiesStrategyFactory {
    @Named("StrategyFromXml")
    LoadFilterConfigurationStrategy createStrategyFromXml(File file);

    @Named("StrategyPredefined")
    LoadFilterConfigurationStrategy createStrategyPredefined();
}
