package com.peliza.properties.loader.strategies;

import com.peliza.properties.model.Configuration;
import com.peliza.properties.model.FilterConfiguration;
import com.peliza.properties.model.Messages;
import com.peliza.properties.model.Property;

import java.util.LinkedList;

public class LoadFilterConfigurationPredefined extends AbstractLoadFilterConfiguration implements LoadFilterConfigurationStrategy {

    final private Configuration configuration;

    public LoadFilterConfigurationPredefined() {
        Configuration configuration = new Configuration();
        LinkedList<FilterConfiguration> filterConfigurations = new LinkedList<>();
        filterConfigurations.add(toPelizaReaderFilterConfiguration());
        filterConfigurations.add(staticFilterConfiguration());
        filterConfigurations.add(aboutUserFilterConfiguration());
        filterConfigurations.add(moodcheckerFilterConfiguration());
        filterConfigurations.add(subjectReminderFilterConfiguration());
        configuration.setFilterConfigurations(filterConfigurations);
        this.configuration = configuration;
    }

    @Override
    public Configuration getConfiguration() {
        return configuration;
    }

    private FilterConfiguration buildFilter(String name) {
        FilterConfiguration filterConfiguration = new FilterConfiguration();
        filterConfiguration.setName(name);
        LinkedList<Messages> messagesList = new LinkedList<>();
        filterConfiguration.setMessages(messagesList);
        return filterConfiguration;
    }

    private FilterConfiguration addMessages(FilterConfiguration filterConfiguration, String id, LinkedList<String> container) {
        Messages messages = new Messages();
        messages.setId(id);
        messages.setMessages(container);
        filterConfiguration.getMessages().add(messages);
        return filterConfiguration;
    }

    private FilterConfiguration staticFilterConfiguration() {
        LinkedList<String> container = new LinkedList<>();
        container.add("Kontynuuj");
        container.add("I?");
        container.add("I jak się z tym czujesz?");
        container.add("Jak to na Ciebie wpłynęło?");

        FilterConfiguration filterConfiguration = buildFilter("StaticFilter");
        addMessages(filterConfiguration, "default", container);

        return filterConfiguration;
    }


    private FilterConfiguration aboutUserFilterConfiguration() {
        LinkedList<String> container = new LinkedList<>();
        container.add("Dlaczego %s %s");
        container.add("Czyli twierdzisz, że %s %s");
        container.add("Chciałabym wiedzieć, dlaczego %s %s?");

        FilterConfiguration filterConfiguration = buildFilter("AboutUserFilter");
        filterConfiguration = addMessages(filterConfiguration, "default", container);


        LinkedList<Property> properties = new LinkedList<>();
        Property nounAndVerbMaxSpace = new Property();
        nounAndVerbMaxSpace.setName("NounAndVerbMaxSpace");
        nounAndVerbMaxSpace.setValue("3");
        properties.add(nounAndVerbMaxSpace);

        filterConfiguration.setProperties(properties);

        return filterConfiguration;
    }

    private FilterConfiguration toPelizaReaderFilterConfiguration() {
        LinkedList<String> container = new LinkedList<>();
        container.add("Nie mówmy o mnie. Skupmy się na tobie");
        container.add("Może porozmawiamy o Tobie.");

        FilterConfiguration filterConfiguration = buildFilter("ToPelizaReaderFilter");
        addMessages(filterConfiguration, "default", container);
        return filterConfiguration;
    }

    private FilterConfiguration moodcheckerFilterConfiguration() {
        LinkedList<String> sadContainer = new LinkedList<>();
        sadContainer.add("Nie martw się! Wszystko będzie dobrze :)");
        sadContainer.add("Czemu jesteś smutny? Życie jest cudowne!");

        LinkedList<String> happyContainer = new LinkedList<>();
        happyContainer.add("Tak trzymaj! Cieszę się, że to słyszę :)");

        FilterConfiguration filterConfiguration = buildFilter("MoodCheckerFilter");
        addMessages(filterConfiguration, "rados", happyContainer);
        addMessages(filterConfiguration, "smut", sadContainer);
        return filterConfiguration;
    }

    private FilterConfiguration subjectReminderFilterConfiguration() {
        LinkedList<String> container = new LinkedList<>();
        container.add("Wrócmy do tematu. ");
        container.add("Wcześniej była mowa o czymś innym. ");

        FilterConfiguration filterConfiguration = buildFilter("SubjectReminderFilter");
        filterConfiguration = addMessages(filterConfiguration, "default", container);

        LinkedList<Property> properties = new LinkedList<>();
        Property waitToRemind = new Property();
        waitToRemind.setName("WaitToRemind");
        waitToRemind.setValue("3");
        properties.add(waitToRemind);

        filterConfiguration.setProperties(properties);
        return filterConfiguration;
    }

}
