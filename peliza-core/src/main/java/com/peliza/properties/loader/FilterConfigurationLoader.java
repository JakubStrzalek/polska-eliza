package com.peliza.properties.loader;

import com.peliza.properties.model.FilterConfiguration;

public interface FilterConfigurationLoader {
    FilterConfiguration loadFilterConfiguration(String filterName);

}
