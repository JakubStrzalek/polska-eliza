package com.peliza;

import com.google.inject.AbstractModule;
import com.peliza.communication.CommunicationModuleConfiguration;
import com.peliza.filters.FiltersModuleConfiguration;
import com.peliza.messages.MessagesModuleConfiguration;

public class CoreInjector extends AbstractModule {
    @Override
    protected void configure() {
        install(new FiltersModuleConfiguration());
        install(new MessagesModuleConfiguration());
        install(new CommunicationModuleConfiguration());
    }


}
