package com.peliza.messages.exceptions;

import com.peliza.utils.ExceptionCommunicates;

/**
 * Created by servlok on 23.05.15.
 */
public class NoSuchPropertyValue extends RuntimeException {

    public NoSuchPropertyValue() {
        super(ExceptionCommunicates.WRONG_INIT_OR_TAG_PROPERTY_TYPE);
    }
}
