package com.peliza.messages.services.transform;


import com.peliza.messages.model.Message;
import com.peliza.messages.model.Word;

import java.io.IOException;
import java.util.Collection;

public interface MessageAnalyzer {
    Collection<Word> parse(Message message) throws IOException;

    Word changeVerbTo2ndFrom1stForm(Word word);
}
