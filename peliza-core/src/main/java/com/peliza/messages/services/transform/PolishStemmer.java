package com.peliza.messages.services.transform;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.peliza.messages.model.Message;
import com.peliza.messages.model.Tag;
import com.peliza.messages.model.Word;
import com.peliza.messages.model.parser.ModelParser;
import com.peliza.messages.model.tagProperties.PersonVerb;
import org.languagetool.AnalyzedTokenReadings;
import org.languagetool.language.Polish;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

@Singleton
public class PolishStemmer implements MessageAnalyzer {

    final private Polish lang;

    final private ModelParser<List<AnalyzedTokenReadings>> modelParser;

    @Inject
    public PolishStemmer(Polish polish, ModelParser<List<AnalyzedTokenReadings>> modelParser) {
        this.lang = polish;
        this.modelParser = modelParser;
    }

    @Override
    public Collection<Word> parse(Message message) throws IOException {
        List<java.lang.String> tokenList = lang.getWordTokenizer().tokenize(message.getOriginal());
        List<AnalyzedTokenReadings> readingses = lang.getTagger().tag(tokenList);
        return modelParser.parse(readingses);
    }

    @Override
    public Word changeVerbTo2ndFrom1stForm(Word word) {
        validateWord(word);
        String original = word.getOriginal();
        String newOriginal = changeFlexion(original);
        word.setOriginal(newOriginal);
        return word;
    }

    private void validateWord(Word word) {
        Tag tag = word.getTag();
        if (tag.getPartOfSpeech() == Tag.PartOfSpeech.VERB
                && tag.getProperty(Tag.Model.personVerb).equals(PersonVerb.p1st.toString())) {
            return;
        }
        throw new IllegalArgumentException();
    }

    private String changeFlexion(String original) {
        if (original.endsWith("em")) {
            return original.replaceAll("em$", "eś");
        }
        if (original.endsWith("łam")) {
            return original.replaceAll("łam$", "łaś");
        }
        if (original.endsWith("am")) {
            return original.replaceAll("am$", "asz");
        }
        if (original.endsWith("ę")) {
            return original.replaceAll("ę$", "sz");
        }

        throw new RuntimeException();
    }
}
