package com.peliza.messages;

import com.google.inject.AbstractModule;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.peliza.messages.model.MessageFactory;
import com.peliza.messages.model.TagFactory;
import com.peliza.messages.model.WordFactory;
import com.peliza.messages.model.parser.AnalyzedReadingTokenInterpreter;
import com.peliza.messages.model.parser.LanguageToolModelParser;
import com.peliza.messages.model.parser.ModelParser;
import com.peliza.messages.model.parser.TokenInterpreter;
import com.peliza.messages.model.parser.mapper.PersonVerbLanguageToolMapper;
import com.peliza.messages.model.parser.mapper.PropertyToModelMapper;
import com.peliza.messages.model.tagProperties.PersonVerb;
import com.peliza.messages.services.transform.MessageAnalyzer;
import com.peliza.messages.services.transform.PolishStemmer;
import org.languagetool.AnalyzedTokenReadings;
import org.languagetool.language.Polish;

import java.util.List;

public class MessagesModuleConfiguration extends AbstractModule {
    @Override
    protected void configure() {
        bind(MessageAnalyzer.class).to(PolishStemmer.class);
        bind(Key.get(new TypeLiteral<ModelParser<List<AnalyzedTokenReadings>>>() {
        }))
                .to(LanguageToolModelParser.class);
        bind(Key.get(new TypeLiteral<TokenInterpreter<AnalyzedTokenReadings>>() {
        }))
                .to(AnalyzedReadingTokenInterpreter.class);

        bind(Key.get(new TypeLiteral<PropertyToModelMapper<PersonVerb>>() {
        }))
                .to(PersonVerbLanguageToolMapper.class);

        FactoryModuleBuilder factoryModuleBuilder = new FactoryModuleBuilder();
        install(factoryModuleBuilder.build(MessageFactory.class));
        install(factoryModuleBuilder.build(TagFactory.class));
        install(factoryModuleBuilder.build(WordFactory.class));

    }

    @Provides
    Polish language() {
        return new Polish();
    }
}
