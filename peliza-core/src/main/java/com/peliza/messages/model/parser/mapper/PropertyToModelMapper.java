package com.peliza.messages.model.parser.mapper;

public interface PropertyToModelMapper<T> {
    T map(String propertyValue);
}
