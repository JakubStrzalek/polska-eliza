package com.peliza.messages.model;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.peliza.messages.services.transform.MessageAnalyzer;

import java.io.IOException;
import java.util.Collection;

public class Message {
    final private String original;
    final private Collection<Word> tokenized;

    @Inject
    public Message(@Assisted String original, MessageAnalyzer messageAnalyzer) throws IOException {
        this.original = original;
        tokenized = messageAnalyzer.parse(this);
    }

    public java.lang.String getOriginal() {
        return original;
    }

    public Collection<Word> getTokenized() {
        return tokenized;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message string1 = (Message) o;

        return !(getOriginal() != null ? !getOriginal().equals(string1.getOriginal()) : string1.getOriginal() != null);

    }

    @Override
    public int hashCode() {
        return getOriginal() != null ? getOriginal().hashCode() : 0;
    }
}
