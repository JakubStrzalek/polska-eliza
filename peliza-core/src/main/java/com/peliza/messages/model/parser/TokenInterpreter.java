package com.peliza.messages.model.parser;

import com.peliza.messages.model.Word;

public interface TokenInterpreter<T> {
    Word interprete(T token);
}
