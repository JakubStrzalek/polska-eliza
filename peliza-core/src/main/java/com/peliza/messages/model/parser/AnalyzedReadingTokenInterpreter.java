package com.peliza.messages.model.parser;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.peliza.messages.model.Tag;
import com.peliza.messages.model.TagFactory;
import com.peliza.messages.model.Word;
import com.peliza.messages.model.WordFactory;
import com.peliza.messages.model.parser.mapper.PropertyToModelMapper;
import com.peliza.messages.model.tagProperties.PersonVerb;
import org.languagetool.AnalyzedToken;
import org.languagetool.AnalyzedTokenReadings;

import java.util.*;

@Singleton
public class AnalyzedReadingTokenInterpreter implements TokenInterpreter<AnalyzedTokenReadings> {

    @Inject
    private WordFactory wordFactory;

    @Inject
    private TagFactory tagFactory;

    @Inject
    private PropertyToModelMapper<PersonVerb> personVerbMapper;

    @Override
    public Word interprete(AnalyzedTokenReadings token) {
        AnalyzedToken analyzedToken = token.getAnalyzedToken(0);
        String lemma = analyzedToken.getLemma();
        String original = analyzedToken.getToken();

        Tag tag = checkIfNullThenParse(analyzedToken.getPOSTag());

        Word word = wordFactory.create(lemma, original, tag);
        return word;
    }

    private Tag checkIfNullThenParse(String posTag) {

        if (posTag == null) {
            return tagFactory.create(Tag.PartOfSpeech.OTHER, new HashMap<>());
        }

        return parseTag(posTag);

    }

    private Tag parseTag(String posTag) {
        List<String> attributes = new LinkedList<>(Arrays.asList(posTag.split(":")));
        String partOfSpeech = attributes.get(0);

        Tag.PartOfSpeech pos = Tag.PartOfSpeech.create(partOfSpeech);

        Map<String, String> properties = interpreteProperties(attributes, pos);

        Tag tag = tagFactory.create(pos, properties);
        return tag;
    }

    private Map<String, String> interpreteProperties(List<String> attributes, Tag.PartOfSpeech pos) {
        Map<String, String> map = new HashMap<>();

        switch (pos) {
            case VERB:
                PersonVerb personVerb = personVerbMapper.map(attributes.get(3));
                map.put(Tag.Model.personVerb, personVerb.toString());
                break;
            case OTHER:
                break;
        }


        return map;
    }

}
