package com.peliza.messages.model;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.peliza.messages.exceptions.NoSuchPropertyValue;

import java.util.Map;

public class Tag {

    final private PartOfSpeech partOfSpeech;
    final private Map<String, String> properties;
    @Inject
    public Tag(@Assisted PartOfSpeech partOfSpeech, @Assisted Map<String, String> properties) {
        this.partOfSpeech = partOfSpeech;
        this.properties = properties;
    }

    public PartOfSpeech getPartOfSpeech() {
        return partOfSpeech;
    }

    public String getProperty(String propertyName) throws NoSuchPropertyValue {
        String propertyValue = properties.get(propertyName);

        if (propertyValue == null)
            throw new NoSuchPropertyValue();

        return propertyValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tag)) return false;

        Tag tag = (Tag) o;

        if (partOfSpeech != tag.partOfSpeech) return false;
        if (properties != null ? !properties.equals(tag.properties) : tag.properties != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = partOfSpeech != null ? partOfSpeech.hashCode() : 0;
        result = 31 * result + (properties != null ? properties.hashCode() : 0);
        return result;
    }

    public enum PartOfSpeech {
        VERB("verb"), NOUN("subst"), OTHER("other");

        private final String text;

        private PartOfSpeech(final String text) {
            this.text = text;
        }

        public static PartOfSpeech create(final String text) {
            switch (text) {
                case "verb":
                    return VERB;
                case "subst":
                    return NOUN;
                default:
                    return OTHER;
            }

        }

        @Override
        public String toString() {
            return text;
        }

    }

    public static class Model {

        public final static String personVerb = "personVerb";
    }
}
