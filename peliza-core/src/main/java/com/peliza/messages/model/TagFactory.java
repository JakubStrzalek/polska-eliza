package com.peliza.messages.model;

import java.util.Map;

public interface TagFactory {
    Tag create(Tag.PartOfSpeech partOfSpeech, Map<String, String> properties);
}
