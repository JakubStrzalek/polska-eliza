package com.peliza.messages.model;

public interface MessageFactory {
    Message create(String original);
}
