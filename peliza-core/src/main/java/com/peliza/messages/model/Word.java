package com.peliza.messages.model;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import javax.annotation.Nullable;

public class Word {
    final private String lemma;
    final private Tag tag;
    private String original;

    @Inject
    public Word(@Assisted("lemma") @Nullable String lemma, @Assisted("original") @Nullable String original, @Assisted Tag tag) {
        this.lemma = lemma;
        this.original = original;
        this.tag = tag;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public Tag getTag() {
        return tag;
    }

    public String getLemma() {
        return lemma;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Word word = (Word) o;

        if (lemma != null ? !lemma.equals(word.lemma) : word.lemma != null) return false;
        if (tag != null ? !tag.equals(word.tag) : word.tag != null) return false;
        return !(original != null ? !original.equals(word.original) : word.original != null);

    }

    @Override
    public int hashCode() {
        int result = lemma != null ? lemma.hashCode() : 0;
        result = 31 * result + (tag != null ? tag.hashCode() : 0);
        result = 31 * result + (original != null ? original.hashCode() : 0);
        return result;
    }
}
