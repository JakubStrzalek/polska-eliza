package com.peliza.messages.model;

import com.google.inject.assistedinject.Assisted;

import javax.annotation.Nullable;

public interface WordFactory {
    Word create(@Assisted("lemma") @Nullable String lemma, @Assisted("original") @Nullable String original, Tag tag);
}
