package com.peliza.messages.model.parser;

import com.peliza.messages.model.Word;

import java.util.List;

public interface ModelParser<I> {
    List<Word> parse(I libraryDependentData);
}
