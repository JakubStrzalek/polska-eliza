package com.peliza.messages.model.parser;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.peliza.messages.model.Word;
import org.languagetool.AnalyzedTokenReadings;

import java.util.LinkedList;
import java.util.List;

@Singleton
public class LanguageToolModelParser implements ModelParser<List<AnalyzedTokenReadings>> {

    @Inject
    private TokenInterpreter<AnalyzedTokenReadings> tokenInterpreter;

    @Override
    public List<Word> parse(List<AnalyzedTokenReadings> libraryDependentData) {
        List<Word> parsed = new LinkedList<>();
        libraryDependentData.stream()
                .forEach((AnalyzedTokenReadings token) -> {
                    if (checkIsTokenized(token))
                        parsed.add(tokenInterpreter.interprete(token));
                });
        return parsed;
    }

    private boolean checkIsTokenized(AnalyzedTokenReadings token) {
        return !token.getToken().trim().isEmpty();
    }


}
