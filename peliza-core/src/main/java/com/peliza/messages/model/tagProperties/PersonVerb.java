package com.peliza.messages.model.tagProperties;

import com.peliza.messages.exceptions.NoSuchPropertyValue;

public enum PersonVerb {
    p1st("first"), p2nd("second"), p3rd("third"), other("other");

    final private String text;

    private PersonVerb(String text) {
        this.text = text;
    }

    public static PersonVerb create(final String text) {
        switch (text) {
            case "first":
                return p1st;
            case "second":
                return p2nd;
            case "third":
                return p3rd;

            default:
                throw new NoSuchPropertyValue();
        }

    }

    @Override
    public String toString() {
        return text;
    }
}
