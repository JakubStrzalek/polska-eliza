package com.peliza.messages.model.parser.mapper;

import com.google.inject.Singleton;
import com.peliza.messages.model.tagProperties.PersonVerb;

@Singleton
public class PersonVerbLanguageToolMapper implements PropertyToModelMapper<PersonVerb> {
    @Override
    public PersonVerb map(String propertyValue) {
        switch (propertyValue) {
            case "pri":
                return PersonVerb.p1st;
            case "sec":
                return PersonVerb.p2nd;
            default:
                return PersonVerb.other;
        }
    }
}
