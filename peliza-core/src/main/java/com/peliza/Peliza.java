package com.peliza;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import com.peliza.communication.request.Request;
import com.peliza.communication.request.RequestFactory;
import com.peliza.communication.session.Session;
import com.peliza.filters.assembler.AssembleType;
import com.peliza.filters.assembler.FilterChainAssembler;
import com.peliza.filters.assembler.FilterChainAssemblerFactory;
import com.peliza.filters.model.Filter;
import com.peliza.messages.model.Message;
import com.peliza.messages.model.MessageFactory;


public class Peliza {

    final private Session<Message> session;

    final private RequestFactory messageRequestFactory;

    final private MessageFactory messageFactory;

    final private Filter filter;

    public Peliza(String fileUrl) {
        Injector injector = Guice.createInjector(new CoreInjector());
        FilterChainAssembler assembler;
        if (fileUrl == null) {
            assembler = injector.getInstance(FilterChainAssemblerFactory.class)
                    .createFilterChainAssembler();
        } else {
            assembler = injector.getInstance(FilterChainAssemblerFactory.class)
                    .createFilterChainAssembler(fileUrl);
        }
        filter = assembler.assemble(AssembleType.PREDEFINED);
        session = injector.getInstance(Key.get(new TypeLiteral<Session<Message>>() {
        }));
        messageRequestFactory = injector.getInstance(RequestFactory.class);
        messageFactory = injector.getInstance(MessageFactory.class);
    }

    public Peliza() {
        this(null);
    }


    public String answer(String string) {
        Message stringToService = messageFactory.create(string);

        Request<Message> request = messageRequestFactory.create(stringToService);

        return filter.service(session, request);

    }

}
