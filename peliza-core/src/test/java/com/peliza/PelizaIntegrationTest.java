package com.peliza;

import com.peliza.communication.request.Request;
import com.peliza.communication.request.RequestFactory;
import com.peliza.communication.session.Session;
import com.peliza.filters.assembler.AssembleType;
import com.peliza.filters.assembler.CompositionFilterChainAssembler;
import com.peliza.filters.assembler.FilterChainBuilder;
import com.peliza.filters.model.Filter;
import com.peliza.filters.model.FiltersFactory;
import com.peliza.messages.model.Message;
import com.peliza.messages.model.MessageFactory;
import com.peliza.properties.loader.FilterConfigurationLoader;
import com.peliza.properties.loader.PropertiesLoaderFactory;
import com.peliza.properties.loader.StubFilterConfigurationLoader;
import org.jukito.JukitoRunner;
import org.jukito.UseModules;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(JukitoRunner.class)
@UseModules(CoreInjector.class)
public class PelizaIntegrationTest {


    private Filter filter;

    private RequestFactory requestFactory;
    private MessageFactory messageFactory;
    private Session<Message> session;

    @Mock
    private PropertiesLoaderFactory propertiesLoaderFactory;

    private FilterConfigurationLoader filterConfigurationLoader = new StubFilterConfigurationLoader();

    @Before
    public void init(Session<Message> session,
                     RequestFactory requestFactory,
                     MessageFactory messageFactory,
                     FiltersFactory filtersFactory,
                     FilterChainBuilder filterChainBuilder) {
        MockitoAnnotations.initMocks(this);

        when(
                propertiesLoaderFactory.create()
        ).thenReturn(filterConfigurationLoader);
        when(
                propertiesLoaderFactory.create(any(String.class))
        ).thenReturn(filterConfigurationLoader);


        CompositionFilterChainAssembler compositionFilterChainAssembler = new CompositionFilterChainAssembler(
                propertiesLoaderFactory,
                filtersFactory,
                filterChainBuilder
        );

        filter = compositionFilterChainAssembler.assemble(AssembleType.PREDEFINED);

        this.session = session;
        this.messageFactory = messageFactory;
        this.requestFactory = requestFactory;
    }

    private String askPeliza(String message) {
        Request<Message> request = requestFactory.create(
                messageFactory.create(message)
        );
        return filter.service(session, request);
    }

    @Test
    public void shouldMoodFilterAnswer() {
        String response = askPeliza("Jestem smutny");
        assertTrue(response.contains("MoodChecker"));
    }

    @Test
    public void shouldAboutUserFilterAnswer() {
        String response = askPeliza("Jestem leniem");
        assertTrue(response.contains("AboutUser"));
    }

    @Test
    public void shouldToPelizaFilterAnswer() {
        String response = askPeliza("Jesteś wkurzająca!");
        assertTrue(response.contains("ToPelizaReader"));
    }

    @Test
    public void shouldStaticFilterAnswer() {
        String response = askPeliza("Nikt nie może mnie sądzić!");
        assertTrue(response.contains("Static"));
    }

    @Test
    public void shouldSubjectReminderAnswer() {
        String response = askPeliza("Jestem leniem");
        assertTrue(response.contains("AboutUser"));
        askPeliza("nic");
        askPeliza("nic");
        askPeliza("nic");
        response = askPeliza("nic");
        assertTrue(response.contains("SubjectReminder"));
    }

}
