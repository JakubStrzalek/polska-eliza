package com.peliza.properties.loader;


import com.peliza.properties.loader.strategies.LoadFilterConfigurationPredefined;
import com.peliza.properties.model.Configuration;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

import static org.junit.Assert.assertNotNull;

public class PropertiesXmlModelTest {

    private Configuration configuration;

    @Before
    public void init() {
        LoadFilterConfigurationPredefined propertiesPredefined = new LoadFilterConfigurationPredefined();
        this.configuration = propertiesPredefined.getConfiguration();

    }

    /**
     * By default ignored (vendore specific)
     * Powinno być @Ingore (w zależnosci od maszyn)
     *
     * @throws JAXBException
     */
    @Test
    @Ignore
    public void shouldCreateValidXmlSetting() throws JAXBException {

        String url = "/home/servlok/Pulpit/result.xml";

        JAXBContext jaxbContext = JAXBContext.newInstance(Configuration.class);
        Marshaller jaxbUnmarshaller = jaxbContext.createMarshaller();

        jaxbUnmarshaller.marshal(this.configuration, new File(url));
    }

    @Test
    @Ignore
    public void shouldImportValidXmlSetting() throws JAXBException {
        String url = "/home/servlok/Projekty/polska-eliza/peliza-console/build/libs/result.xml";
        JAXBContext jaxbContext = JAXBContext.newInstance(Configuration.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        Configuration configuration = (Configuration) jaxbUnmarshaller.unmarshal(new File(url));
        assertNotNull(configuration);


    }
}
