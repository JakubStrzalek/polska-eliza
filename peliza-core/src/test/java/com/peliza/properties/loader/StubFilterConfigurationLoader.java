package com.peliza.properties.loader;

import com.peliza.properties.model.FilterConfiguration;
import com.peliza.properties.model.Messages;
import com.peliza.properties.model.Property;

import java.util.LinkedList;

public class StubFilterConfigurationLoader implements FilterConfigurationLoader {
    @Override
    public FilterConfiguration loadFilterConfiguration(String filterName) {
        FilterConfiguration filterConfiguration = new FilterConfiguration();
        LinkedList<Messages> messages = null;
        switch (filterName) {
            case "ToPelizaReaderFilter":
                messages = toPelizaReaderFilterConfiguration();
                break;
            case "StaticFilter":
                messages = staticFilterConfiguration();
                break;
            case "AboutUserFilter":
                messages = aboutUserFilterConfiguration();
                break;
            case "MoodCheckerFilter":
                messages = moodcheckerFilterConfiguration();
                break;
            case "SubjectReminderFilter":
                messages = subjectReminderFilterConfiguration();
                break;
        }
        if (messages == null || messages.isEmpty())
            throw new IllegalArgumentException();

        filterConfiguration.setMessages(messages);
        defaultPropertiesConfiguration(filterConfiguration);
        return filterConfiguration;
    }

    private void defaultPropertiesConfiguration(FilterConfiguration filterConfiguration) {
        LinkedList<Property> properties = new LinkedList<>();
        Property nounAndVerbMaxSpace = new Property();
        nounAndVerbMaxSpace.setName("NounAndVerbMaxSpace");
        nounAndVerbMaxSpace.setValue("3");
        properties.add(nounAndVerbMaxSpace);
        Property waitToRemind = new Property();
        waitToRemind.setName("WaitToRemind");
        waitToRemind.setValue("3");
        properties.add(waitToRemind);

        filterConfiguration.setProperties(properties);
    }


    private LinkedList<Messages> subjectReminderFilterConfiguration() {
        LinkedList<Messages> messages = new LinkedList<>();
        Messages defaultMessages = new Messages();
        defaultMessages.setId("default");
        LinkedList<String> container = new LinkedList<>();
        container.add("SubjectReminder Here! I remind you last succesfull filter");
        defaultMessages.setMessages(container);
        messages.add(defaultMessages);
        return messages;
    }

    private LinkedList<Messages> moodcheckerFilterConfiguration() {
        LinkedList<Messages> messages = new LinkedList<>();
        Messages stemmedMessages = new Messages();
        stemmedMessages.setId("smut");
        LinkedList<String> container = new LinkedList<>();
        container.add("MoodChecker Here! You typed sth with 'smut' in it.");
        stemmedMessages.setMessages(container);
        messages.add(stemmedMessages);
        return messages;
    }

    private LinkedList<Messages> aboutUserFilterConfiguration() {
        LinkedList<Messages> messages = new LinkedList<>();
        Messages defaultMessages = new Messages();
        defaultMessages.setId("default");
        LinkedList<String> container = new LinkedList<>();
        container.add("AboutUser Here! You're response contains %s %s");
        defaultMessages.setMessages(container);
        messages.add(defaultMessages);
        return messages;
    }

    private LinkedList<Messages> staticFilterConfiguration() {
        LinkedList<Messages> messages = new LinkedList<>();
        Messages defaultMessages = new Messages();
        defaultMessages.setId("default");
        LinkedList<String> container = new LinkedList<>();
        container.add("Static Here!");
        defaultMessages.setMessages(container);
        messages.add(defaultMessages);
        return messages;
    }

    private LinkedList<Messages> toPelizaReaderFilterConfiguration() {
        LinkedList<Messages> messages = new LinkedList<>();
        Messages defaultMessages = new Messages();
        defaultMessages.setId("default");
        LinkedList<String> container = new LinkedList<>();
        container.add("ToPelizaReader Here! You typed sth regarding user");
        defaultMessages.setMessages(container);
        messages.add(defaultMessages);
        return messages;
    }
}
