package com.peliza.filters.model;

import com.peliza.CoreInjector;
import com.peliza.communication.request.RequestFactory;
import com.peliza.communication.session.Session;
import com.peliza.messages.model.Message;
import com.peliza.messages.model.MessageFactory;
import org.jukito.JukitoRunner;
import org.jukito.UseModules;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JukitoRunner.class)
@UseModules(CoreInjector.class)
public class MoodCheckerFilterIntegrationTest extends FilterIntegrationTest {
    private static final String TO_BE_EXPECTED_MESSAGE = "Nie martw się";

    @Before
    public void init(RequestFactory requestFactory, MessageFactory messageFactory, Session<Message> session) {
        super.init(requestFactory, messageFactory, null, session, "Nie martw się", "smut");


        filterToBeTested = this.moodCheckerFilter();
    }

    @Test
    public void shouldReturnMessage() {
        //before
        String response = createMessage("Przepełnia mnie smutek");

        //except
        assertEquals(TO_BE_EXPECTED_MESSAGE, response);
    }
}
