package com.peliza.filters.model;

import com.peliza.CoreInjector;
import com.peliza.communication.request.RequestFactory;
import com.peliza.communication.session.Session;
import com.peliza.messages.model.Message;
import com.peliza.messages.model.MessageFactory;
import org.jukito.JukitoRunner;
import org.jukito.UseModules;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JukitoRunner.class)
@UseModules(CoreInjector.class)
public class StaticFilterIntegrationTest extends FilterIntegrationTest {

    private static final String TO_BE_EXPECTED_MESSAGE = "Udało się hurra!";

    @Before
    public void init(RequestFactory requestFactory, MessageFactory messageFactory, Session<Message> session) {
        super.init(requestFactory, messageFactory, null, session, TO_BE_EXPECTED_MESSAGE, DEFAULT_KEY);

        filterToBeTested = staticFilter();
    }

    @Test
    public void shouldReturnMessage() {
        //before
        String response = createMessage("Słuchasz mnie?");

        //except
        assertEquals(TO_BE_EXPECTED_MESSAGE, response);
    }
}
