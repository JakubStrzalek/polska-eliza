package com.peliza.filters.model;

import com.peliza.CoreInjector;
import com.peliza.communication.request.RequestFactory;
import com.peliza.communication.session.Session;
import com.peliza.messages.model.Message;
import com.peliza.messages.model.MessageFactory;
import com.peliza.messages.services.transform.MessageAnalyzer;
import org.jukito.JukitoRunner;
import org.jukito.UseModules;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(JukitoRunner.class)
@UseModules(CoreInjector.class)
public class AboutUserFilterIntegrationTest extends FilterIntegrationTest {

    @Before
    public void init(RequestFactory requestFactory, MessageFactory messageFactory, MessageAnalyzer messageAnalyzer, Session<Message> session) {
        super.init(requestFactory, messageFactory, messageAnalyzer, session, "Dlaczego %s %s?", DEFAULT_KEY);

        when(
                configuration.loadProperty(any(String.class))
        ).thenReturn("3");

        filterToBeTested = this.aboutUserFilter();

    }

    @Test
    public void shouldReturnMessage() {
        //before
        String response = createMessage("Lubię lody");

        //except
        assertEquals("Dlaczego lubisz lody?", response);
    }

    /**
     * Equals return null
     */
//    @Test(expected = IllegalStateException.class)
//    public void shouldPopagateToNext() {
//        //before
//        String toBeExpected = "Udało się hurra!";
//
//        Message message = messageFactory.create("Słucham się");
//        Request<Message> request = messageRequestFactory.create(message);
//
//        //invoke
//        String response = filter.service(null, request);
//
//    }
}
