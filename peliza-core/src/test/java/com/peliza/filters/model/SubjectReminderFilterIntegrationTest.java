package com.peliza.filters.model;

import com.peliza.CoreInjector;
import com.peliza.communication.request.RequestFactory;
import com.peliza.communication.session.Session;
import com.peliza.messages.model.Message;
import com.peliza.messages.model.MessageFactory;
import org.jukito.JukitoRunner;
import org.jukito.UseModules;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(JukitoRunner.class)
@UseModules(CoreInjector.class)
public class SubjectReminderFilterIntegrationTest extends FilterIntegrationTest {

    private static final String TO_BE_EXPECTED_MESSAGE = "Udało się hurra!";
    private Filter subjectReminderFilter;

    @Before
    public void init(RequestFactory requestFactory, MessageFactory messageFactory, Session<Message> session) {
        super.init(requestFactory, messageFactory, null, session, TO_BE_EXPECTED_MESSAGE, DEFAULT_KEY);

        when(
                configuration.loadProperty(any(String.class))
        ).thenReturn("3");

    }


    @Test(expected = IllegalStateException.class)
    public void shouldSubjectReminderPropagateToNext() {
        filterToBeTested = StubFilterFactory.alwaysPropagateFilter();
        Filter filter = subjectReminderFilter();
        filterToBeTested.setNext(filter);
        filter.setPrev(filterToBeTested);

        createMessage("Słuchasz mnie?");
    }

    @Test
    public void shouldReturnRemindMessageAfter3Propagation() {
        String[] states = {"Response!", null, null, null, null};


        filterToBeTested = StubFilterFactory.answerOnStateContainerDependent(Arrays.asList(states));
        Filter filter = subjectReminderFilter();
        filterToBeTested.setNext(filter);
        filter.setPrev(filterToBeTested);
        Filter staticFilter = StubFilterFactory.constantAnswerFilter();
        filter.setNext(staticFilter);
        staticFilter.setPrev(filter);


        //except
        String response = createMessage("Słuchasz mnie?");
        assertEquals("Response!", response);
        response = createMessage("Słuchasz mnie?");
        assertEquals(StubFilterFactory.STUB_ANSWER, response);
        response = createMessage("Słuchasz mnie?");
        assertEquals(StubFilterFactory.STUB_ANSWER, response);
        response = createMessage("Słuchasz mnie?");
        assertEquals(StubFilterFactory.STUB_ANSWER, response);
        response = createMessage("Słuchasz mnie?");
        assertEquals(TO_BE_EXPECTED_MESSAGE + "Response!", response);
    }
}
