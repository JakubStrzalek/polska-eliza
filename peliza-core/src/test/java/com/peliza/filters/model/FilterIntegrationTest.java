package com.peliza.filters.model;

import com.peliza.communication.request.Request;
import com.peliza.communication.request.RequestFactory;
import com.peliza.communication.session.Session;
import com.peliza.filters.model.statefull.SubjectReminderFilter;
import com.peliza.messages.model.Message;
import com.peliza.messages.model.MessageFactory;
import com.peliza.messages.services.transform.MessageAnalyzer;
import com.peliza.properties.loader.FilterConfigurationLoader;
import com.peliza.properties.model.FilterConfiguration;
import com.peliza.utils.RandomListAccess;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public abstract class FilterIntegrationTest {

    protected static String DEFAULT_KEY = "default";

    protected Filter filterToBeTested;

    @Mock
    protected FilterConfigurationLoader loader;

    @Mock
    protected FilterConfiguration configuration;

    protected RequestFactory messageRequestFactory;
    protected MessageFactory messageFactory;
    protected MessageAnalyzer messageAnalyzer;

    protected Session<Message> session;

    public void init(RequestFactory requestFactory,
                     MessageFactory messageFactory,
                     MessageAnalyzer messageAnalyzer,
                     Session<Message> session,
                     String expectedMessage,
                     String keyToMap) {
        this.messageRequestFactory = requestFactory;
        this.messageFactory = messageFactory;
        this.messageAnalyzer = messageAnalyzer;
        this.session = session;

        List<String> expectedResponses = new LinkedList<>();
        expectedResponses.add(expectedMessage);

        Map<String, RandomListAccess<String>> map = new HashMap<>();
        map.put(keyToMap, new RandomListAccess<>(expectedResponses));

        MockitoAnnotations.initMocks(this);

        when(
                loader.loadFilterConfiguration(any(String.class))
        ).thenReturn(configuration);

        when(
                configuration.loadCustomResponses()
        ).thenReturn(map);

        if (keyToMap.equals("default")) {
            when(
                    configuration.loadResponses()
            ).thenReturn(new RandomListAccess<>(expectedResponses));
        }

    }

    protected Filter moodCheckerFilter() {
        return new MoodCheckerFilter(loader);
    }

    protected Filter staticFilter() {
        return new StaticFilter(loader);
    }

    protected Filter toPelizaReaderFilter() {
        return new ToPelizaReaderFilter(loader);
    }

    protected Filter subjectReminderFilter() {
        return new SubjectReminderFilter(loader);
    }

    protected Filter aboutUserFilter() {
        return new AboutUserFilter(loader, messageAnalyzer);
    }

    protected String createMessage(String value) {
        Message message = messageFactory.create(value);
        Request<Message> request = messageRequestFactory.create(message);

        return filterToBeTested.service(session, request);
    }

}
