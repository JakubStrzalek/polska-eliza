package com.peliza.filters.model;

import com.peliza.communication.request.Request;
import com.peliza.communication.session.Session;
import com.peliza.messages.model.Message;

import java.util.Iterator;
import java.util.List;

public class StubFilterFactory {

    public static final String STUB_ANSWER = "Stub Answer";

    protected static Filter alwaysPropagateFilter() {
        return new AbstractFilter() {

            @Override
            protected String invoke(Session<Message> session, Request<Message> request) {
                return null;
            }
        };
    }

    protected static Filter constantAnswerFilter() {
        return new AbstractFilter() {

            @Override
            protected String invoke(Session<Message> session, Request<Message> request) {
                return STUB_ANSWER;
            }
        };
    }

    protected static Filter answerOnStateContainerDependent(List<String> states) {
        return new StatedFilter(states);
    }

    private static class StatedFilter extends AbstractFilter {
        private Iterable<String> answers;
        private Iterator<String> currentState;

        StatedFilter(Iterable<String> answerStates) {
            super();
            this.answers = answerStates;
            this.currentState = answerStates.iterator();
        }

        @Override
        protected String invoke(Session<Message> session, Request<Message> request) {
            if (!currentState.hasNext()) {
                this.currentState = answers.iterator();
            }
            return currentState.next();
        }
    }
}
