package com.peliza.filters.assembler;

import com.peliza.filters.FiltersModuleConfiguration;
import com.peliza.filters.model.Filter;
import org.jukito.JukitoRunner;
import org.jukito.UseModules;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JukitoRunner.class)
@UseModules(FiltersModuleConfiguration.class)
public class CompositionFilterChainAssemblerIntegrationTest {

    private FilterChainAssembler filterChainAssembler;

    @Before
    public void init(FilterChainAssemblerFactory filterChainAssemblerFactory) {
        this.filterChainAssembler = filterChainAssemblerFactory.createFilterChainAssembler();
    }

    @Test
    public void shouldPredefinedAssemblance() {
        Filter filter = filterChainAssembler.assemble(AssembleType.PREDEFINED);
        assertEquals(filter.getClass().getSimpleName(), "MoodCheckerFilter");
        filter = filter.getNext();
        assertEquals(filter.getClass().getSimpleName(), "AboutUserFilter");
        filter = filter.getNext();
        assertEquals(filter.getClass().getSimpleName(), "SubjectReminderFilter");
        filter = filter.getNext();
        assertEquals(filter.getClass().getSimpleName(), "ToPelizaReaderFilter");
        filter = filter.getNext();
        assertEquals(filter.getClass().getSimpleName(), "StaticFilter");
    }
}
