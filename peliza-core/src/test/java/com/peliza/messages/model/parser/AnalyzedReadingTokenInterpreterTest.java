package com.peliza.messages.model.parser;


import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import com.peliza.messages.MessagesModuleConfiguration;
import com.peliza.messages.exceptions.NoSuchPropertyValue;
import com.peliza.messages.model.Tag;
import com.peliza.messages.model.Word;
import com.peliza.messages.model.tagProperties.PersonVerb;
import org.junit.Before;
import org.junit.Test;
import org.languagetool.AnalyzedTokenReadings;
import org.languagetool.language.Polish;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class AnalyzedReadingTokenInterpreterTest {

    private TokenInterpreter<AnalyzedTokenReadings> tokenInterpreter;

    private Polish lang;

    @Before
    public void init() {
        Injector injector = Guice.createInjector(new MessagesModuleConfiguration());
        tokenInterpreter = injector.getInstance(Key.get(new TypeLiteral<TokenInterpreter<AnalyzedTokenReadings>>() {
        }));
        lang = injector.getInstance(Polish.class);

    }

    private Word prepareWord(String toPrepare) throws IOException {
        List<String> analyzedTokens = lang.getWordTokenizer().tokenize(toPrepare);
        List<AnalyzedTokenReadings> tokenList = lang.getTagger().tag(analyzedTokens);
        AnalyzedTokenReadings analyzedToken = tokenList.get(0);

        Word word = tokenInterpreter.interprete(analyzedToken);
        return word;
    }

    @Test
    public void shouldParseVerbWithSecPerson() throws IOException, NoSuchPropertyValue {
        Word word = prepareWord("chcesz");

        assertEquals(word.getTag().getPartOfSpeech(), Tag.PartOfSpeech.VERB);
        assertEquals(PersonVerb.p2nd.toString(), word.getTag().getProperty(Tag.Model.personVerb));
    }

    @Test
    public void shouldParseOther() throws IOException {
        Word word = prepareWord("samochód");

        assertEquals(word.getTag().getPartOfSpeech(), Tag.PartOfSpeech.NOUN);

    }

    @Test(expected = NoSuchPropertyValue.class)
    public void shouldExpectNoSuchProperty() throws IOException, NoSuchPropertyValue {
        Word word = prepareWord("samochód");

        assertNull(word.getTag().getProperty(Tag.Model.personVerb));
    }
}
