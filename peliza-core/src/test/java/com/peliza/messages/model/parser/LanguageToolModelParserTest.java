package com.peliza.messages.model.parser;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import com.peliza.messages.MessagesModuleConfiguration;
import com.peliza.messages.model.Word;
import org.junit.Before;
import org.junit.Test;
import org.languagetool.AnalyzedTokenReadings;
import org.languagetool.language.Polish;

import java.io.IOException;
import java.util.List;

public class LanguageToolModelParserTest {

    List<AnalyzedTokenReadings> toTest;
    private ModelParser<List<AnalyzedTokenReadings>> languageToolModelParser;

    @Before
    public void init() throws IOException {
        Injector injector = Guice.createInjector(new MessagesModuleConfiguration());
        languageToolModelParser = injector.getInstance(Key.get(new TypeLiteral<ModelParser<List<AnalyzedTokenReadings>>>() {
        }));

        Polish lang = injector.getInstance(Polish.class);
        List<String> tokenList = lang.getWordTokenizer().tokenize("chcesz chcę chciałem kup kupuję kupowałem ");
        toTest = lang.getTagger().tag(tokenList);
    }

    @Test
    public void shouldThen1() {

        List<Word> words = languageToolModelParser.parse(toTest);

        System.out.print("done");
    }
}
