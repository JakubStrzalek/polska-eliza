package com.peliza.communication.session;

import com.peliza.CoreInjector;
import com.peliza.communication.request.Request;
import com.peliza.communication.request.RequestFactory;
import com.peliza.filters.model.AbstractFilter;
import com.peliza.filters.model.Filter;
import com.peliza.messages.model.Message;
import com.peliza.messages.model.MessageFactory;
import org.jukito.JukitoRunner;
import org.jukito.UseModules;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(JukitoRunner.class)
@UseModules(CoreInjector.class)
public class SessionLastFilterAnswerTest {

    private Session<Message> messageSession;

    private MessageFactory messageFactory;
    private RequestFactory requestFactory;


    public String createMessageAndService(String value, Filter filter) {
        Message message = messageFactory.create(value);
        Request<Message> request = requestFactory.create(message);

        return filter.service(messageSession, request);
    }

    public Filter stubAbstractFilter(String answer) {
        return new AbstractFilter() {
            @Override
            protected String invoke(Session<Message> session, Request<Message> request) {
                return answer;
            }
        };
    }

    public Filter stubAbstractFilter() {
        return stubAbstractFilter("Stub Answer");
    }

    @Before
    public void init(Session<Message> messageSession, MessageFactory messageFactory, RequestFactory requestFactory) {
        this.messageSession = messageSession;
        this.messageFactory = messageFactory;
        this.requestFactory = requestFactory;
    }

    @Test
    public void shouldBeFilterInLastAnswers() {
        String response = createMessageAndService("First question", stubAbstractFilter());
        String lastAnswer = messageSession.getLastAnswer(stubAbstractFilter());

        assertEquals(lastAnswer, response);

    }

    @Test
    public void shouldBeNullInLastAnswers() {

        String lastAnswer = messageSession.getLastAnswer(stubAbstractFilter());

        assertNull(lastAnswer);
    }

    @Test
    public void shouldChangeLastAnswers() {
        String firstResponse = createMessageAndService("First question", stubAbstractFilter("First"));
        String secondResponse = createMessageAndService("Second question", stubAbstractFilter("Second"));

        String lastAnswer = messageSession.getLastAnswer(stubAbstractFilter());
        assertEquals(lastAnswer, secondResponse);
        assertNotEquals(lastAnswer, firstResponse);
    }


}
