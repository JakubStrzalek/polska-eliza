package com.peliza.communication.request;

import com.peliza.CoreInjector;
import com.peliza.messages.model.Message;
import com.peliza.messages.services.transform.MessageAnalyzer;
import org.jukito.JukitoRunner;
import org.jukito.UseModules;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(JukitoRunner.class)
@UseModules(CoreInjector.class)
public class MessageRequestTest {

    private Request<Message> request;

    private RequestFactory requestFactory;

    @Mock
    private MessageAnalyzer messageAnalyzer;


    private Message makeMessage(String original) throws IOException {
        return new Message(original, messageAnalyzer);
    }

    @Before
    public void init(RequestFactory requestFactory) throws IOException {
        this.requestFactory = requestFactory;

        MockitoAnnotations.initMocks(this);

        when(
                messageAnalyzer.parse(any(Message.class))
        ).thenReturn(
                new LinkedList<>()
        );

    }

    @Test
    public void shouldDoAnything() throws IOException {
        Message message = makeMessage("Anything");

        request = requestFactory.create(message);

        assertEquals(request.getRequestedObject(), message);
    }



}
