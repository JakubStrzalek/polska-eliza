package test;

import org.junit.Before;
import org.junit.Test;
import org.languagetool.AnalyzedTokenReadings;
import org.languagetool.language.Polish;

import java.io.IOException;
import java.util.List;

public class StemmingIntegrationTest {


    private Polish lang;

    @Before
    public void init() {
        this.lang = new Polish();
    }


    //    @Test
//    public void shouldCheckStemming2() throws IOException {
//        Stemmer stemmer = new Stemmer();
//        String result = stemmer.stem("dom", false);
//        System.out.print("Stem: " + result);
//    @Test
//    public void shouldCheckStemming() {
//        String word = "słucha";
//        Dictionary polish = Dictionary.getForLanguage("pl");
//        DictionaryLookup dl = new DictionaryLookup(polish);
//        List<WordData> wordList = dl.lookup(word);
//        WordData wordData = wordList.get(0);
//        System.out.print("Tag :" + wordData.getTag() + "\n");
//        System.out.print("Stem :" + wordData.getStem() + "\n");
//        System.out.print("Word :" + wordData.getWord() + "\n");
//        System.out.print("ToString :" + wordData.toString() + "\n");
//    }
//
    @Test
    public void shouldCheckStemming3() throws IOException {
        List<String> tokenList = lang.getWordTokenizer().tokenize("słuchasz mnie?");
        List<AnalyzedTokenReadings> list = lang.getTagger().tag(tokenList);
        System.out.print("ToString :");
    }
}
